#include <stdio.h>
int main()
{
	int st, fn, act, i=0;
	printf("Enter a range in format value1-value2: ");
	if (scanf("%ud-%ud", &st, &fn)==1 || st<0 || fn<0)
	{
		printf("Error! Incorrect input!\n");				//�������� �� ������������ �����
		return 1;
	}
	act=(st<fn)? 1: -1;								        //��� �� ����������� ������������� ��� ������������� ��� ��� ��
	printf("Result: [");
	for (st; st!=fn; st+=act)								//� ��� ������, ����� ��������� � ��������� ������-�� �����
		printf("%d,",st);
	printf("%d]\n", fn);
	return 0;
}